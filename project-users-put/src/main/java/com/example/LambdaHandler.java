package com.example;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class LambdaHandler implements RequestHandler<User, User> {

    @Override
    public User handleRequest(User user, Context context) {
        DynamoDBMapper dbMapper = new DynamoDBMapper(AmazonDynamoDBClientBuilder.defaultClient());
        if (dbMapper.load(User.class, user.getId()) != null) {
            dbMapper.save(user);
            return user;
        }
        else return null;
    }
}
