package com.example;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;

public class LambdaHandler implements RequestHandler<Object, String> {

    @Override
    public String handleRequest(Object object, Context context) {
        return publishMessageToTopic();
    }

    private String publishMessageToTopic(){
        SnsClient snsClient = SnsClient.builder()
                .region(Region.US_EAST_2)
                .build();
        PublishRequest publishRequest = PublishRequest.builder()
                .topicArn("arn:aws:sns:us-east-2:977795155242:my-users-sns")
                .subject("WELCOME PEOPLE")
                .message("Welcome to SNS for email!")
                .build();
        return "Send message successfully with id: " + snsClient.publish(publishRequest).messageId();
    }
}
