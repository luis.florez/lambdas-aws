package com.example;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class LambdaHandler implements RequestHandler<Integer, String> {

    @Override
    public String handleRequest(Integer id, Context context) {
        DynamoDBMapper dbMapper = new DynamoDBMapper(AmazonDynamoDBClientBuilder.defaultClient());
        User user = dbMapper.load(User.class, id);
        if (user != null) {
            dbMapper.delete(user);
            return "Deleted";
        }
        else return "Not deleted";
    }
}
